# Output variables
output "autoscalegroup_id" {
  value = "${aws_autoscaling_group.jenkins.id}"
}

output autoscalegroup_name {
  value = "${aws_autoscaling_group.jenkins.name}"
}

# Input variables

#Define a name for this runners
variable name {
  default = "tf"
}

variable jenkins_label {
  default = ""
}

#Define in which subnets we want to boot instances into
variable vpc_subnets {
  default = ""
}

# Define the vpc zones
variable vpc_zones {
  default = ""
}

# What is the minimum capacity of our runner cluster
variable min_size {
  default = "1"
}

# What is our maximum capacity
variable max_size {
  default = "2"
}

# what is our desired capacity
variable capacity {
  default = "1"
}

# What type of instances we want to boot into our cluster
variable instance_type {
  default = "t2.small"
}

# Which is our Image Amis (We assume we use ubuntu for our runners)
variable image_id {
  default = "ami-26e70c49" # 16.04 LTS	amd64	hvm:ebs-ssd	20160627
}

# If we want to allow public ip to the runners
variable public_ip_address {
  default = "true"
}

# Coma seperated list of security groups to assign to the instances
variable security_groups {
  default = ""
}

# What is the disk we want the runners to have
variable volume_size {
  default = "100"
}

# What is the EBS volume type
variable volume_type {
  default = "gp2"
}

# EBS should be cleaned up uppon termination, tidy up
variable volume_delete {
  default = "true"
}

# How many executors we want to have for the worker, this is based on the type
variable worker_executor {
  default = "2"
}

# What is the jenkins credential id that we use to establish connection.
variable jenkins_credential_id {
  default = "jenkins-runner"
}

# What is the Jenkins master url
variable jenkins_master {
  default = ""
}

# What is the user that can use the Jenkins remote api
variable jenkins_user {
  default = ""
}

# What is the pass of that user
variable jenkins_pass {
  default = ""
}

# SSH public key that we want to use in order to access the worker machine later.
variable master_ssh_public_key {
  default = ""
}

# AWS Keypair name
variable key_name {
  default = ""
}

resource "aws_autoscaling_group" "jenkins" {
  vpc_zone_identifier       = ["${ split(",",var.vpc_subnets) }"]
  availability_zones        = ["${ split(",",var.vpc_zones) }"]
  name                      = "Jenkins-${var.name}"
  min_size                  = "${var.min_size}"
  max_size                  = "${var.max_size}"
  desired_capacity          = "${var.capacity}"
  health_check_grace_period = 15
  health_check_type         = "EC2"
  launch_configuration      = "${aws_launch_configuration.jenkins_slave_lc.name}"

  tag {
    key                 = "Name"
    value               = "${var.name} runner"
    propagate_at_launch = true
  }

  tag {
    key                 = "Role"
    value               = "Runner"
    propagate_at_launch = false
  }

  tag {
    key                 = "Group"
    value               = "CI"
    propagate_at_launch = false
  }
}

resource "aws_launch_configuration" "jenkins_slave_lc" {
  name_prefix                 = "jenkins-runner-"
  image_id                    = "${var.instance_type}"
  instance_type               = "${var.image_id}"
  associate_public_ip_address = "${var.public_ip_address}"
  key_name                    = "${var.key_name}"
  user_data                   = "${template_file.jenkins_slave.rendered}"
  security_groups             = ["${split(",", var.security_groups)}"]

  root_block_device {
    volume_size           = "${var.volume_size}"
    volume_type           = "${var.volume_type}"
    delete_on_termination = "${var.volume_delete}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "template_file" "jenkins_slave" {
  template = <<TPL
#cloud-config
repo_update: true
repo_upgrade: all

packages:
 - jq
 - default-jdk
 - gettext
 - apt-transport-https
 - ca-certificates

groups:
  - docker
  - jenkins

ssh_authorized_keys:
  - ${SSH_KEY}

write_files:
  - path: /etc/apt/sources.list.d/docker.list
    content: |
     deb https://apt.dockerproject.org/repo ubuntu-xenial main

  - path: /opt/add_node.xml
    owner: ubuntu:ubuntu
    permissions: '0755'
    content: |
     <slave>
      <name>${HOST_NAME}</name>
      <description>${INSTANCE_ID}</description>
      <remoteFS>/opt/worker</remoteFS>
      <numExecutors>${WORKER_EXECUTOR}</numExecutors>
      <mode>NORMAL</mode>
      <retentionStrategy class="hudson.slaves.RetentionStrategy$Always"/>
      <launcher class="hudson.plugins.sshslaves.SSHLauncher" plugin="ssh-slaves@1.5">
        <host>${HOST_IP}</host>
        <port>22</port>
        <credentialsId>${JENKINS_CREDENTIAL_ID}</credentialsId>
      </launcher>
      <label>${JENKINS_LABEL}</label>
      <nodeProperties/>
      <userId>ubuntu</userId>
     </slave>

runcmd:
  - [ mkdir, -p, /opt/worker ]
  - [ sudo, chown, ubuntu.ubuntu, /opt/, -R ]
  - [ sh, -c, "HOST_IP=$(curl http://169.254.169.254/latest/meta-data/public-ipv4) HOST_NAME=$(curl http://169.254.169.254/latest/meta-data/hostname) INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id) envsubst < /opt/add_node.xml > /opt/node.xml" ]
  - [ apt-key, "adv", "--keyserver", "hkp://p80.pool.sks-keyservers.net:80", "--recv-keys", "58118E89F3A912897C070ADBF76221572C52609D" ]
  - [ apt-cache, policy, docker-engine ]
  - [ apt-get, install, docker-engine, --allow-unauthenticated, -y ]
  - [ service, docker, start ]
  - [ systemctl, enable, docker ]
  - [ groupadd, docker ]
  - [ usermod, -aG, docker, ubuntu ]
  - [ sh, -c, "curl -L https://github.com/docker/compose/releases/download/1.7.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose" ]
  - [ sh, -c, "chmod +x /usr/local/bin/docker-compose" ]
  - [ sh, -c, "wget ${JENKINS_JAR_URL}" ]
  - [ sh, -c, "cat /opt/node.xml | java -jar jenkins-cli.jar -s ${JENKINS_MASTER} create-node $HOST_NAME --username ${JENKINS_USER} --password ${JENKINS_PASS}" ]
  - [ export, JAVA_HOME="/usr/lib/jvm/default-java/bin" ]
TPL

  vars {
    HOST_NAME             = "$${HOST_NAME}"
    INSTANCE_ID           = "$${INSTANCE_ID}"
    HOST_IP               = "$${HOST_IP}"
    WORKER_EXECUTOR       = "${var.worker_executor}"
    JENKINS_CREDENTIAL_ID = "${var.jenkins_credential_id}"
    JENKINS_MASTER        = "${var.jenkins_master}"
    JENKINS_USER          = "${var.jenkins_user}"
    JENKINS_PASS          = "${var.jenkins_pass}"
    JENKINS_LABEL         = "${var.name},${var.jenkins_label}"
    SSH_KEY               = "${var.master_ssh_public_key}"
  }

  lifecycle {
    create_before_destroy = true
  }
}
