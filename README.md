# Jenkins runner terraform module

The reason behind this project is to deploy an AWS autoscaling group with Jenkins runners.

This uses terraform and to boot the machine and configure the runner.

> This is a work in progress proceed if you know what you are doing.

That the module does is provision an ubuntu server then install docker, docker-compose and tries to be automatically register with a Jenkins master server.

## Pending Todo

 - Make docker version and docker-compose version configurable though module variables.

## Example

### Simple usage
```
module "jenkins-runner" {
    source = "bitbucket.org/alexsapran/aws-jenkins-runner"

    name                  = "ci"
    vpc_subnets           = "..."
    min_size              = 1
    max_size              = 2
    capacity              = 1
    instance_type         = "t2.medium"
    # Use the default ami from eu-central-1a
    # image_id            = "ami-26e70c49"
    security_groups       = "${aws_security_group.jenkins.id}, ${aws_security_group.some_other.id}"

    jenkins_credential_id = "jenkins-runner"
    jenkins_master        = "http://master-jenkins.mycompany.ltd"
    jenkins_user          = "jenkins-user"
    jenkins_pass          = "awsome_password"
    master_ssh_public_key = ${var.ssh_key}
}

```

### With scalling runners

```
resource "aws_autoscaling_policy" "jenkins_runner_up" {
  name                   = "jenkins_runner_up"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = "${module.jenkins-runner.autoscalegroup_name}"
}

resource "aws_autoscaling_policy" "jenkins_runner_down" {
  name                   = "jenkins_runner_down"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = "${module.jenkins-runner.autoscalegroup_name}"
}

resource "aws_cloudwatch_metric_alarm" "runner_cpu_up" {
  alarm_name          = "runner_alarm_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "300"
  statistic           = "Average"
  threshold           = "90"

  dimensions {
    AutoScalingGroupName = "${module.jenkins-runner.autoscalegroup_name}"
  }

  alarm_actions = [
    "${aws_autoscaling_policy.jenkins_runner_up.arn}",
  ]
}
```
